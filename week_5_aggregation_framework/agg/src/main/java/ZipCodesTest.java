import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import org.bson.Document;
import org.bson.conversions.Bson;

import javax.management.openmbean.ArrayType;
import javax.print.Doc;
import java.util.*;
import java.util.logging.Filter;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.gte;
import static java.util.Arrays.asList;
import static java.util.Arrays.setAll;

/**
 * Created by eandwrz on 2017-04-28.
 */
public class ZipCodesTest {
	public static void main(String[] args) {

		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("agg");
		MongoCollection<Document> collection = db.getCollection("zips");

//		List <Document> pipeline = asList(new Document("$group",
//		                                         new Document("_id", "$state")
//		  .append("totalPop", new Document("$sum", "$pop"))),
//		  new Document("$match", new Document("totalPop", new Document("$gte", 1000000)))
//		);

		List<Bson> pipeline = asList(group("$state", sum("totalPop", "$pop")),
		  match(gte("totalPop", 1000000))
		  );

		List<Document> results = collection.aggregate(pipeline).into(new ArrayList<Document>());

		for (Document doc : results) {
			System.out.println(doc.toJson());
		}
	}
}
