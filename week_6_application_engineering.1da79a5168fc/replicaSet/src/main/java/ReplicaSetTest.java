import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import static java.util.Arrays.asList;

/**
 * Created by eandwrz on 2017-05-01.
 */
public class ReplicaSetTest {
	public static void main(String[] args) throws InterruptedException {

		MongoClient client = new MongoClient(asList(new ServerAddress("localhost", 27017),
		  new ServerAddress("localhost", 27018),
		  new ServerAddress("localhost", 27019)),
		                                      MongoClientOptions.builder()
			                                    .requiredReplicaSetName("rs1")
			                                    .build());
		MongoCollection<Document> collection = client.getDatabase("course").getCollection("replication");
		collection.drop();

		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			try {
				collection.insertOne(new Document("_id", i));
				System.out.println("Inserted new document " + i);
			} catch (MongoException e) {
				System.out.println("Exception inserting document " + i + " : " + e.getMessage());;
			}
			Thread.sleep(500);
		}
	}
}
