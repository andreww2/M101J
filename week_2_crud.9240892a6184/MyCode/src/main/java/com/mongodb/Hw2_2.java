package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.ascending;

/**
 * Created by Andy on 2017-04-12.
 */
public class Hw2_2 {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("students");
        MongoCollection<Document> grades = db.getCollection("grades");

        MongoCursor <Document> cursor = grades.find(eq("type", "homework")).sort(ascending("student_id", "score")).iterator();

        Object studentId = -1;

        try {
            while(cursor.hasNext()){
                Document entry = cursor.next();
                if(!entry.get("student_id").equals(studentId)) {
                    Object id = entry.get("_id");
                    System.out.println("Deleting " + entry);
                    grades.deleteOne(eq("_id", id));

                }
                studentId = entry.get("student_id");
            }
        } finally {
            cursor.close();
        }
    }
}

//
//
//
//
//    MongoCursor<Document> cursor = grades.find(eq("type", "homework"))
//            .sort(ascending("student_id", "score")).iterator();
//
//    Object studentId = -1;
//        try{
//                while(cursor.hasNext()) {
//                Document entry = cursor.next();
//                if(!entry.get("student_id").equals(studentId)) {
//                Object id = entry.get("_id");
//                System.out.println("Deletion of " + entry);
//                grades.deleteOne(eq("_id", id));
//                }
//                studentId = entry.get("student_id");
//                }
//                } finally {
//                cursor.close();
//                }