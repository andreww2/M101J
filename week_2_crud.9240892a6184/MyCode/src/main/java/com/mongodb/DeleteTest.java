package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;

import static com.mongodb.Helpers.printJson;
import static com.mongodb.client.model.Filters.gt;

/**
 * Created by Andy on 2017-04-10.
 */
public class DeleteTest {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("course");
        MongoCollection<Document> collection = db.getCollection("test");

        collection.drop();

        for (int i = 0; i < 8; i++) {
            collection.insertOne(new Document("_id", i));
        }

        collection.deleteMany(gt("_id", 4));

        for (Document cur : collection.find().into(new ArrayList<Document>())) {
            printJson(cur);
        }
    }
}
