package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.Helpers.printJson;

/**
 * Created by Andy on 2017-04-08.
 */
public class FindTest {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("course");
        MongoCollection<Document> coll = db.getCollection("findTest");

        coll.drop();

        for (int i = 0; i < 10; i++) {
            coll.insertOne(new Document("x", i));
        }

        System.out.println("Find one:");
        Document first = coll.find().first();
        printJson(first);

        System.out.println("Find all with into:");
        List<Document> all = coll.find().into(new ArrayList<Document>());
        for(Document cur : all) {
            printJson(cur);
        }
        System.out.println("Find all with interaction:");
        MongoCursor <Document> cursor = coll.find().iterator();
        try{
            while(cursor.hasNext()) {
                printJson(cursor.next());
            }
        }
        finally {
            cursor.close();
        }

        System.out.println("Count:");
        long count = coll.count();
        System.out.println(count);
    }
}
