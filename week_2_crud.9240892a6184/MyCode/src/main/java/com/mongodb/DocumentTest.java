package com.mongodb;

import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.Arrays;
import java.util.Date;

import static com.mongodb.Helpers.printJson;

/**
 * Created by Andy on 2017-04-08.
 */
public class DocumentTest {
    public static void main(String[] args) {
        Document document = new Document()
                .append("str", "mongoDB Hello")
                .append("int", 42)
                .append("long", 1L)
                .append("double", 2.123)
                .append("bool", false)
                .append("date", new Date())
                .append("objectId", new ObjectId())
                .append("null", null)
                .append("embeddedDoc", new Document("x", 0))
                .append("list", Arrays.asList(1,2,3));

        String str = document.getString("str");
        int i = document.getInteger("int");

        printJson(document);

        BsonDocument bsonDocument = new BsonDocument("str", new BsonString("MongoDB, Hello"));

    }
}
