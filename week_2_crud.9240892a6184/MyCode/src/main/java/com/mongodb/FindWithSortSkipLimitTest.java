package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.Helpers.printJson;
import static com.mongodb.client.model.Projections.*;
import static com.mongodb.client.model.Sorts.*;

/**
 * Created by Andy on 2017-04-09.
 */
public class FindWithSortSkipLimitTest {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("course");
        MongoCollection <Document> collection = db.getCollection("FindWithSortSkipLimitTest");

        collection.drop();

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                collection.insertOne(new Document().append("i", i).append("j", j));
            }
        }

        Bson projectionDoc = fields(include("i", "j"), excludeId());
        //Bson sortDoc = new Document("i", 1).append("j", -1);
        Bson sortDoc = orderBy(ascending("i"), descending("j"));

        List<Document> all = collection.find()
                .projection(projectionDoc)
                .sort(sortDoc)
                .limit(50)
                .skip(20)
                .into(new ArrayList<Document>());




        for (Document cur : all) {
            printJson(cur);
        }
    }

}
