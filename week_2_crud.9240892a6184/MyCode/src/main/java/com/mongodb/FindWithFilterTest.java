package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.mongodb.Helpers.printJson;
import static com.mongodb.client.model.Filters.*;

/**
 * Created by Andy on 2017-04-09.
 */
public class FindWithFilterTest {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("course");
        MongoCollection<Document> coll = db.getCollection("findWithFilterTest");

        coll.drop();

        for (int i = 0; i < 10; i++) {
            coll.insertOne(new Document()
                    .append("x", new Random().nextInt(2))
                    .append("y", new Random().nextInt(100)));
        }

//        Bson filter = new Document("x", 0)
//                .append("y", new Document("$gt", 10)
//                        .append("$lt", 90));

        //This is an alternative way
        Bson filter = and(eq("x", 0),
                gt("y", 10),
                lt("y", 90));

        List<Document> all = coll.find(filter).into(new ArrayList<Document>());

        for (Document cur : all) {
            printJson(cur);
        }

        long count = coll.count(filter);
        System.out.println();
        System.out.println(count);


    }
}
