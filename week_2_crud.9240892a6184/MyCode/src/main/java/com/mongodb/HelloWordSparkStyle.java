package com.mongodb;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

/**
 * Created by Andy on 2017-04-01.
 */
public class HelloWordSparkStyle {
    public static void main(String[] args) {
        Spark.get(("/"), new Route() {
            public Object handle(Request request, Response response) throws Exception {
                return "Hello World From Spark";
            }
        });
    }
}