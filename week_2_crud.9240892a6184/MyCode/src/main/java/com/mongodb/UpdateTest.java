package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;

import java.util.ArrayList;

import static com.mongodb.Helpers.printJson;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.lte;
import static com.mongodb.client.model.Updates.*;

/**
 * Created by Andy on 2017-04-10.
 */
public class UpdateTest {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("course");
        MongoCollection<Document> collection = db.getCollection("test");

        collection.drop();

        for (int i = 0; i < 8; i++) {
            collection.insertOne(new Document()
                    .append("_id", i)
                    .append("x", i)
                    .append("y", true));
        }


        //Replace
        collection.replaceOne(eq("x", 5), new Document("x", 20).append("updated", true));
        //Update
        collection.updateOne(eq("x", 6), new Document("$set", new Document("x", 21).append("updated", true)));
        collection.updateOne(eq("x", 7), combine(set("x", 22), set("updated", true)));
        //Upsert
        collection.updateOne(eq("_id", 8), combine(set("x", 23), set("upserted", true)),
                new UpdateOptions().upsert(true));
        //Update Many
        collection.updateMany(lte("x", 4), inc("x", 1));


        for (Document cur : collection.find().into(new ArrayList<Document>())) {
            printJson(cur);
        }
    }
}
