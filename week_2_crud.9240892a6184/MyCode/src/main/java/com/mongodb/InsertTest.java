package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import static java.util.Arrays.asList;

/**
 * Created by Andy on 2017-04-08.
 */
public class InsertTest {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("course");
        MongoCollection<Document> coll = db.getCollection("course");

        coll.drop();

        Document smith = new Document("name", "Smith")
                .append("age", 30)
                .append("proffession", "programmer");

        Document jones = new Document("name", "Jones")
                .append("age", 28)
                .append("proffession", "programmer");

        //Before insertion, while printing smith doc there is no ID, after insertion, Java Driver creates one and prints
        coll.insertMany(asList(smith, jones));

    }
}
