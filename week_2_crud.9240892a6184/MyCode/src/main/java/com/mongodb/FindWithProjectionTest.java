package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.mongodb.Helpers.printJson;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Projections.*;

/**
 * Created by Andy on 2017-04-09.
 */
public class FindWithProjectionTest {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("course");
        MongoCollection<Document> collection = db.getCollection("findWithProjection");

        collection.drop();

        for (int i = 0; i < 10; i++) {
            collection.insertOne(new Document()
                    .append("x", new Random().nextInt(2))
                    .append("y", new Random().nextInt(100))
                    .append("i", i));
        }

        Bson filterDoc = and(eq("x", 0), gt("y", 10), lt("y", 90));

        //Exclusion
        //Bson projectionDoc = new Document("x", 0).append("_id", 0);
        //Inclusion
        //Bson projectionDoc = new Document("y", 1).append("i", 1).append("_id",0);
        //Using PrjectionsClass
        Bson projectionDoc = fields(exclude("_id"), include("y", "i"));
        //Projections.excludeId(); <= works the same

        List<Document> all = collection.find(filterDoc)
                .projection(projectionDoc)
                .into(new ArrayList<Document>());

        for (Document cur :
                all) {
            printJson(cur);
        }

    }
}
