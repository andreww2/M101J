import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by eandwrz on 2017-05-06.
 */
public class DeleteOrphans {
	public static void main(String[] args) {

		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("final7");
		MongoCollection<Document> albumCollection = db.getCollection("albums");
		MongoCollection<Document> imagesCollection = db.getCollection("images");

		List<Document> albumList = albumCollection.find().into(new ArrayList<Document>());
		List<Document> imagesList = imagesCollection.find().projection(eq("_id", 1)).into(new ArrayList<Document>());


//		for (Document img : imagesList) {
//			boolean found = false;
//			Object imgId = img.get("_id");
//			for (Document alb : albumList) {
//				List<Integer> imgListInAlbum = (List<Integer>) alb.get("images");
//				for (int isImgInAlbum : imgListInAlbum) {
//					if (imgId.equals(isImgInAlbum)) {
//						found = true;
//					}
//				}
//			}
//			if (!found) {
//				imagesCollection.deleteOne(img);
//				System.out.println("Deleted an orphan");
//			}
//		}
	}
}
