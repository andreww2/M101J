package course;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.List;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by Andy on 2017-04-19.
 */
public class RemoveLowest {
	public static void main(String[] args) {

		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("school");
		MongoCollection<Document> grades = db.getCollection("students");

		MongoCursor<Document> cursor = grades.find().iterator();
		try {
			while (cursor.hasNext()) {
				Document student = cursor.next();
				List<Document> scores = (List<Document>) student.get("scores");
				Document scoreMinObj = null;
				double scoreMin = Double.MAX_VALUE;
				for (Document scoreDocument : scores) {
					double score = scoreDocument.getDouble("score");
					String type = scoreDocument.getString("type");
					if (type.equals("homework") && score < scoreMin) {
						scoreMin = score;
						scoreMinObj = scoreDocument;
					}
				}
				if (scoreMinObj != null) {
					scores.remove(scoreMinObj);
				}
				grades.updateOne(eq("_id", student.get("_id")),
				  new Document("$set",new Document("scores", scores)));
			}
		} finally {
			cursor.close();
		}
		client.close();
	}
}


