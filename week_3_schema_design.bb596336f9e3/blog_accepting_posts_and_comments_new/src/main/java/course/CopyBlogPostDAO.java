package course;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import org.bson.Document;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by eandwrz on 2017-04-22.
 */
public class CopyBlogPostDAO {
	private MongoCollection<Document> postsCollection;

	CopyBlogPostDAO(final MongoDatabase blogDatabase) {
		postsCollection = blogDatabase.getCollection("posts");
	}

	public Document findByPermalink(String permalink) {
		Document post = postsCollection.find(eq("permalink", permalink)).first();
		return post;
	}

	public List<Document> findByDateDescending(int limit) {
		List<Document> posts = postsCollection.find()
		  .sort(Sorts.descending("date"))
		  .limit(limit)
		  .into(new ArrayList<Document>());
		return posts;
	}

	public String addPost(String title, String body, List tags, String username) {
		System.out.println("Inserting blog entry " + title + " " + body);

		String permalink = title.replaceAll("\\s", "_");
		permalink.replaceAll("\\W", "");
		permalink.toLowerCase();

		Document post = new Document("title", title)
		  .append("author", username)
		  .append("body", body)
		  .append("tags", tags)
		  .append("permalink", permalink)
		  .append("comments", new ArrayList<String>())
		  .append("date", new Date());

		postsCollection.insertOne(post);

		return permalink;
	}

	public void addPostComment(final String name, final String email, final String body, final String permalink) {

		Document comment = new Document("author", name)
		  .append("body", body);
		if (email != null && email != "") {
			comment.append("email", email);
		}
		postsCollection.updateOne(eq("permalink", permalink),
		  Updates.push("comments", comment));
//		new Document("$push", new Document("comments", comment)));
	}

}
